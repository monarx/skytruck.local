(function ($) {
	Drupal.behaviors.close_modal = {
		attach: function(context, settings) {
			$('#modal-backdrop, .modal-close, #modal .continue').on('click',function(e){
				$('#modal-overlay').remove();
				e.preventDefault();
			});

			$('#cboxClose, #cboxOverlay').on('click', function(){
				$('.protectionOverlayHref').remove();
			});

			$('.navbar-overlay').on('click', function(e){
		  		$(this).remove();
		  		$('#navbar-main > ul > li').removeClass('active');
		  		e.preventDefault();
		  	});
		}
	};

	Drupal.behaviors.location_map = {
    attach:function (context, settings) {
      $('.views-field-field-location-map a').colorbox({
      	onComplete: function(){
      		$('#cboxContent').append('<a href="#" class="colorbox-print"><i class="icon-print"></i>Распечатать</a>');

      		$('#cboxContent .colorbox-print').on('click',function(e){ // Printed location map
		      	var html_to_print = $('#cboxLoadedContent').html();
		      	var iframe=$('<iframe id="print_frame">');
		      	$('body').append(iframe);
		      	var doc = $('#print_frame')[0].contentDocument || $('#print_frame')[0].contentWindow.document;
		        var win = $('#print_frame')[0].contentWindow || $('#print_frame')[0];
		        doc.getElementsByTagName('body')[0].innerHTML = html_to_print;
		        win.print();
		        $('#print_frame').remove();
		      	e.preventDefault();
		      });
      	},
      	onClosed: function(){
      		$('.colorbox-print').remove();
      	}
      });

    }
	};	

	Drupal.behaviors.enable_spinner = {
    attach:function (context, settings) {
        $('.quantity input, input.spinner', context).spinner({min: 1, max: 99});
    }
	};

	Drupal.behaviors.search_example = {
    attach:function (context, settings) {
      $('.parts-search-example span').on('click', function(){
      	var text = $(this).text();
      	$(this).parents('form').find('input[type="text"]').val(text);
      });
    }
	};

	Drupal.behaviors.replacement_label = {
    attach:function (context, settings) {
    	$(window).load(function(){
	    	if ($('.replacements .view-label').length > 0) {
		      $('.replacements .view-label').each(function(){
		      	var parent = $(this).parent();
		      	var $parent_height = parent.height() - 29;
		      	$(this).width($parent_height);
		      });
		    }
		  });
    }
	};

  Drupal.behaviors.main_menu = {	
  	attach: function(context, settings) {
  		$(document).ready(function(){
  			var hash = window.location.hash;
		    if (hash == '#manufacturer') {
		    	console.log(hash);
		    	console.log($('.header-after .tab-nav a').last());
		        $('.header-after .tab-nav .blue a').last().click(); 
		    }
  		});

	  	$('#navbar-main > ul > li > a').once(function(){
	  		$(this).live('click', function(e){
				if ($(this).parent().hasClass('active')) {
				  	$(this).parent().removeClass('active');
				  	$('.navbar-overlay').remove();
				} else {
				  	$('#navbar-main > ul > li').removeClass('active');
				  	$('body').append('<div class="navbar-overlay"></div>');
				  	$(this).parent().addClass('active');
				}
			  	e.preventDefault();
		  	});
	  	});

  	  	$('.navbar-overlay').live('click',function(){
  	  		$(this).remove();
	  		$('#navbar-main > ul > li').removeClass('active');
	  	});
  	}
  };

  Drupal.behaviors.imageProtector = {
    attach: function (context, settings) {
    $('img').bind('contextmenu', function(e) {
      return false;
    });
    /*
		var pixelSource = '/assets/i/Transparent.gif';
	    var useOnAllImages = false;
	    // Preload the pixel
	    var preload = new Image();
	    preload.src = pixelSource;

	    $('.node-detail .left .field-name-field-image img, #cboxLoadedContent img').live('mouseenter touchstart', function(e) {
	        // Only execute if this is not an overlay or skipped
	        var img = $(this);
	        if (useOnAllImages && ($('.field-name-field-detail-photo img').length > 0)) return;
	        if (img.parent().attr('href'))
	        	var url = img.parent().attr('href');
	        else 
	        	var url = img.attr('src');

	        // Get the real image's position, add an overlay
	        var pos = img.offset();

	        var title = "";
	        if ($(this).parent().is('#cboxLoadedContent')) {
	        	title = $('#cboxTitle').text();
	        } else {
		        if (img.attr('title')) {
		        	title = img.attr('title');
		        }	        	
	        }

	        if ($(this).parent().is('#cboxLoadedContent')) {
		        var overlay = 
		        	$('<span href="' + url + '" class="protectionOverlayHref" title="' + title + '" rel><img class="protectionOverlay" src="' + pixelSource + '"  width="' + img.width() + '" height="' + img.height() + '" alt="' + title + '" /></span>')
		        	.css({
		        		position: 'absolute', 
		        		zIndex: 9999999, 
		        		left: pos.left, 
		        		top: pos.top, 
		        		width: img.width(),
		        		height: img.height(),
		        		display: 'block',
		        		margin: '-20px 0 0 0'
		        	})
		        	.appendTo('body')
		        	.bind('mouseleave', function() {
		            setTimeout(function(){ overlay.remove(); 
		          }, 0, $(this));
		        });
	        } else {
		        var overlay = 
		        	$('<a href="' + url + '" class="protectionOverlayHref colorbox init-colorbox-processed cboxElement" title="' + title + '" rel><img class="protectionOverlay" src="' + pixelSource + '"  width="' + img.width() + '" height="' + img.height() + '" alt="' + title + '" /></a>')
		        	.css({
		        		position: 'absolute', 
		        		zIndex: 9999999, 
		        		left: pos.left, 
		        		top: pos.top, 
		        		width: img.width(),
		        		height: '330px',
		        		display: 'block',
		        		margin: '-40px 0 0'
		        	})
		        	.appendTo('body')
		        	.bind('mouseleave', function() {
		            setTimeout(function(){ overlay.remove(); 
		          }, 0, $(this));
		      	});
		      }
	        if ('ontouchstart' in window) $(document).one('touchend', function(){ setTimeout(function(){ overlay.remove(); }, 0, overlay); });
	    });*/
    }
  };

})(jQuery);
