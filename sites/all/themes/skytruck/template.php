<?php
// $Id: template.php,v 1.13 2010/12/14 01:04:27 dries Exp $

/**
 * Implements hook_html_head_alter().
 * This work only without Metatag module.
 * @param $head_elements
 */
function skytruck_html_head_alter(&$head_elements) {
  unset($head_elements['system_meta_generator']);
  foreach ($head_elements as $key => $element) {
    if (isset($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'alternate') {
      unset($head_elements[$key]);
    }
    if (isset($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'canonical') {
      unset($head_elements[$key]);
    }
    if (isset($element['#attributes']['rel']) && $element['#attributes']['rel'] == 'shortlink') {
      unset($head_elements[$key]);
    }
  }
}

/**
 * Implements template_preprocess_html().
 * Add body classes if certain regions have content.
 * @param $variables
 */
function skytruck_preprocess_html(&$variables) {
  $href = 'http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic-ext,cyrillic';
  $google_font_properties = array(
    'rel' => 'stylesheet',
    'type' => 'text/css',
    'href' => $href,
  );
  drupal_add_html_head_link($google_font_properties, TRUE);

  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  drupal_add_library('system', 'ui.spinner');
}

/**
 * Implements hook_preprocess_page().
 * See page.tpl.php.
 * @param $variables
 */
function skytruck_preprocess_page(&$variables) {
  //Brand icon in header.
  $brand_aliases = array();
  if ($brand = taxonomy_vocabulary_machine_name_load('brand')) {
    $terms = taxonomy_get_tree($brand->vid);
    foreach ($terms as $term) {
      $brand_aliases[] = drupal_get_path_alias('taxonomy/term/' . $term->tid);
    }
  }

  $first_segment = arg(0, ltrim($_SERVER['REQUEST_URI'], '/'));

  $variables['brand_logo'] = '';
  if (in_array($first_segment, $brand_aliases) && is_file(path_to_theme() . '/images/header_logo/' . $first_segment . '.png')) {
    $variables['brand_logo'] = theme_image(array('path' => path_to_theme() . '/images/header_logo/' . $first_segment . '.png', 'attributes' => array('class' => array('brand-logo-img'))));
  }

  // Format and add main menu to theme
  $main_menu_tree = menu_tree_all_data('main-menu');
  $variables['main_menu'] = menu_tree_output($main_menu_tree);

  // Format and add main menu to theme
  $manufacturer_menu_tree = menu_tree_all_data('menu-manufacturer');
  $variables['manufacturer_menu'] = menu_tree_output($manufacturer_menu_tree);

  // Format and add top menu to theme
  $top_menu_tree = menu_tree_all_data('menu-top-menu');
  $variables['top_menu'] = menu_tree_output($top_menu_tree);

  // Format and add top menu to theme
  $user_menu_tree = menu_tree_all_data('user-menu');
  $variables['user_menu'] = menu_tree_output($user_menu_tree);

  //Output taxonomy catalog tabs.
  if (isset($variables['page']['content']['system_main']['term_heading']['term']['#bundle']) && $variables['page']['content']['system_main']['term_heading']['term']['#bundle'] == 'brand') {
    $term = $variables['page']['content']['system_main']['term_heading']['term']['#term'];
    if ($parents = taxonomy_get_parents($term->tid)) {
      $parent = reset($parents);
      $childrens = taxonomy_get_children($parent->tid);
      $variables['page']['content']['system_main']['term_heading']['#prefix'] .= '<ul class="catalog-tabs">';
      foreach($childrens as $children) {
        $link_name = ($children->tid == 29)?t('Howo with 2007'):$children->name;
        $variables['page']['content']['system_main']['term_heading']['#prefix'] .= '<li class="catalog-tabs-term-'.$children->tid .'">' . l('<div class="medium primary button"><span>' . $link_name . '</span></div>', 'taxonomy/term/' . $children->tid, array('html' => TRUE)) . '</li>';
      }
      $variables['page']['content']['system_main']['term_heading']['#prefix'] .= '</ul>';
    }
  }

  $status = drupal_get_http_header("status");
  if ($status == "404 Not Found") {
    $variables['theme_hook_suggestions'][] = 'page__404';
  }
}

/**
 * Implements theme_menu_tree().
 * @param $variables
 * @return string
 */
function skytruck_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements hook_commerce_price_formatted_components().
 * @param $vars
 * @return string
 */
function skytruck_commerce_price_formatted_components($vars) {
  $vars['components']['commerce_price_formatted_amount']['title'] = t('Total:');
  return theme_commerce_price_formatted_components($vars);
}

/**
 * Implements hook_menu_link().
 * Customize menu.
 * @param array $variables
 * @return string
 */
function skytruck_menu_link($variables) {
  $element = $variables['element'];
  $sub_menu = '';

  $options = $element['#localized_options'];
  $options['html'] = true;
  //$options['attributes'] = $element['#attributes'];

  if (($element['#below']) && ($element['#original_link']['depth'] == 1)) {
    $sub_menu = '<div class="dropdown"><div class="row">' . drupal_render($element['#below']) . '</div></div>';
  } else {
    $sub_menu =  drupal_render($element['#below']);
  }

  //Theme menu icon.
  $menu_icon = '';
  if (isset($element['#original_link']['options']['menu_icon']) && $element['#original_link']['options']['menu_icon']['enable']) {
    $menu_icon = theme('image_style',
      array(
        'style_name' => $element['#original_link']['options']['menu_icon']['image_style'],
        'path' => $element['#original_link']['options']['menu_icon']['path'],
      )
    );
    $menu_icon = '<i class="icon">' . $menu_icon . '</i>';
  }

  $output = l($menu_icon . '<span>' . $element['#title'] . '</span>', $element['#href'], $options);
  if ((isset($element['#below']['#children'])) && ($element['#original_link']['depth'] == 2)) {
    array_push($element['#attributes']['class'], 'level-two');
    return '<li' . drupal_attributes($element['#attributes']) . '>' . '<span class="link">' . $output  . '</span>' . $sub_menu . "</li>\n";
  } else 
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Implements theme_file_link().
 * @param $variables
 * @return string
 */
function skytruck_file_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];
  $mb = format_size($file->filesize);
  $mime = strtr($file->filemime, array('/' => '-', '.' => '-'));
  $url = file_create_url($file->uri);

  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );

  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }

  return '<span class="file ' . $mime . '"><span class="icon"><span class="size">' . $mb . '</span></span>' . l($link_text, $url, $options) . '</span>';
}

/**
 * Implements theme_pager().
 * @param $variables
 * @return mixed
 */
function skytruck_pager($variables) {
	return preg_replace('/<h2.*?>.*?<\/h2>/is', '', theme_pager($variables));
}

/**
 * Overrides theme_form_element().
 * @param $variables
 * @return string
 */
function skytruck_form_element(&$variables) {
  $element = &$variables['element'];

  $is_checkbox = FALSE;
  $is_radio = FALSE;
  $is_password = FALSE;

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }

  // Check for errors and set correct error class.
  if (isset($element['#parents']) && form_get_error($element)) {
    $attributes['class'][] = 'error';
  }

  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type ' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item ' . strtr($element['#name'], array(
      ' ' => '-',
      '_' => '-',
      '[' => '-',
      ']' => '',
    ));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  if (!empty($element['#autocomplete_path']) && drupal_valid_path($element['#autocomplete_path'])) {
    $attributes['class'][] = 'form-autocomplete';
  }
  $attributes['class'][] = 'field';


  if (isset($element['#type'])) {
    if ($element['#type'] == "radio") {
      $attributes['class'][] = 'radio';
      $is_radio = TRUE;
    }
    elseif ($element['#type'] == "checkbox") {
      $attributes['class'][] = 'checkbox';
      $is_checkbox = TRUE;          
    }
    elseif ($element['#type'] == "password") {
      $attributes['class'][] = 'text input password';
      $is_password = TRUE;
    }    
    else {
      $attributes['class'][] = 'form-group';
    }
  }


  $description = FALSE;
  $tooltip = FALSE;
  // Convert some descriptions to tooltips.
  // @see gumby_tooltip_descriptions setting in _gumby_settings_form()

  if (!empty($element['#description'])) {
    $description = $element['#description'];
   // if (theme_get_setting('gumby_tooltip_enabled') && theme_get_setting('gumby_tooltip_descriptions') && $description === strip_tags($description) && strlen($description) <= 200) {
      //$tooltip = TRUE;
      //kpr($attributes['class']);
      $attributes['class'] = 'field ttip';
      //$attributes['data-tooltip'] = $description;
   // }
  }


  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }


$prefix = '';
  $suffix = '';
  if (isset($element['#field_prefix']) || isset($element['#field_suffix'])) {
    // Determine if "#input_group" was specified.
    if (!empty($element['#input_group'])) {
      $prefix .= '<div class="input-group">';
      $prefix .= isset($element['#field_prefix']) ? '<span class="adjoined">' . $element['#field_prefix'] . '</span>' : '';
      $suffix .= isset($element['#field_suffix']) ? '<span class="adjoined">' . $element['#field_suffix'] . '</span>' : '';
      $suffix .= '</div>';
    }
    else {
      $prefix .= isset($element['#field_prefix']) ? $element['#field_prefix'] : '';
      $suffix .= isset($element['#field_suffix']) ? $element['#field_suffix'] : '';
    }
  }

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      if ($is_radio || $is_checkbox) {
        $output .= ' ' . $prefix . $element['#children'] . $suffix;
      }
      else {
        $variables['#children'] = ' ' . $prefix . $element['#children'] . $suffix;
      }
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if ($description && !$tooltip) {
    $output .= '<p class="help-block">' . $element['#description'] . "</p>\n";
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Implements theme_button().
 * @param $variables
 * @return string
 */
function skytruck_button($variables) {
  $element = $variables['element'];
  $label = $element['#value'];
  element_set_attributes($element, array('id', 'name', 'value', 'type'));
  
  // If a button type class isn't present then add in default.
  $button_classes = array(
    'default',
    'primary',
    'success',
    'info',
    'warning',
    'danger',
    'link',
  );
  $class_intersection = array_intersect($button_classes, $element['#attributes']['class']);
  if (empty($class_intersection)) {
    $element['#attributes']['class'][] = 'default';
  }

  // Add in the button type class.
  $element['#attributes']['class'][] = 'form ' . $element['#button_type'];

  // This line break adds inherent margin between multiple buttons.
  return '<button' . drupal_attributes($element['#attributes']) . '>' . $label . "</button>\n";
}

/**
 * Implements hook_preprocess_button().
 * @param $vars
 */
function skytruck_preprocess_button(&$variables) {
  $variables['element']['#attributes']['class'][] = 'medium primary';
}

function skytruck_field($variables) {
  $output = '';
  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}