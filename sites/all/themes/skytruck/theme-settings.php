<?php

/**
 * @file:
 * Define custom theme settings.
 */
function skytruck_form_system_theme_settings_alter(&$form) {
    $form['additional_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Additional settings'),
    );
    $form['additional_settings']['email'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Email'),
        '#default_value' => theme_get_setting('email'),
        '#description'   => t("Please specify email to contact with you."),
    );
    $form['additional_settings']['phone'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Main site phone'),
        '#default_value' => theme_get_setting('phone'),
        '#description'   => t("Please specify commonly used phone number to contact with you."),
    );
    $form['additional_settings']['twiter'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Twiter url'),
        '#default_value' => theme_get_setting('twiter'),
    );
    $form['additional_settings']['youtube'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Youtube url'),
        '#default_value' => theme_get_setting('youtube'),
    );
}