<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="<?php print $language->language; ?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="<?php print $language->language; ?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="<?php print $language->language; ?>"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="<?php print $language->language; ?>"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 9]><!--> <html class="no-js" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>> <!--<![endif]-->

<head profile="<?php print $grddl_profile; ?>">
	<meta name="viewport" content="width=1020">
	<?php print $head; ?>

	<title><?php print $head_title; ?></title>
	<?php print $styles; ?>
	<?php print $scripts; ?>

  <!-- Yandex.Metrika Marked Phone -->
  <script type="text/javascript"
  src="//mc.yandex.ru/metrika/phone.js?counter=9409030"
  defer="defer"></script>
  <!-- /Yandex.Metrika Marked Phone -->  	
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <!-- Yandex.Metrika counter -->
  <div style="display:none;"><script type="text/javascript">
  (function(w, c) {
      (w[c] = w[c] || []).push(function() {
          try {
              w.yaCounter9409030 = new Ya.Metrika({id:9409030, enableAll: true, webvisor:true});
          }
          catch(e) { }
      });
  })(window, "yandex_metrika_callbacks");
  </script></div>
  <script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript" defer="defer"></script>
  <noscript><div><img src="//mc.yandex.ru/watch/9409030" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
  <!-- /Yandex.Metrika counter -->

  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
<div class="counters">
  <script type="text/javascript"><!--LiveInternet counter-->
    <!--
      document.write("<a href='http://www.liveinternet.ru/click' "+
      "target=_blank><img src='//counter.yadro.ru/hit?t44.4;r"+
      escape(document.referrer)+((typeof(screen)=="undefined")?"":
      ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
      screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
      ";"+Math.random()+
      "' alt='' title='LiveInternet' "+
      "border='0' width='31' height='31'><\/a>")
    //-->
  </script> 				<!--/LiveInternet-->
  
  <div class="rambler"><!-- begin of Top100 code -->  
    <script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?2626230"></script> 
    <noscript>
    <a href="http://top100.rambler.ru/navi/2626230/">
    </a>
    </noscript> <!-- end of Top100 code -->
  </div>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-15760549-5', 'skytruck.ru');
    ga('send', 'pageview');
  </script>
  
  
  <script src='http://scan.botscanner.com/'></script> <noscript><img src='http://scan.botscanner.com/noscript' /></noscript>  
  
  <!-- BEGIN JIVOSITE CODE {literal} -->
  <script type='text/javascript'>
  (function(){ var widget_id = '129728';
  var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
  <!-- {/literal} END JIVOSITE CODE -->
</div>
</body>
</html>