<?php

/**
 * @file
 * Default implementation of the shopping cart block template.
 *
 * Available variables:
 * - $contents_view: A rendered View containing the contents of the cart.
 *
 * Helper variables:
 * - $order: The full order object for the shopping cart.
 *
 * @see template_preprocess()
 * @see template_process()
 */
?>
<div id="basket">
  <?php if (!empty($order)): ?>
    <a href="/cart">
      <div class="body">
        <?php if (isset($quantity)): ?>
          <div class="count">
            <?php print format_plural($quantity, '@count product', '@count products'); ?>
          </div>
        <?php endif; ?>
        <?php if (isset($total)): ?>
          <div class="total">
            <?php print t('Total @total', array('@total' => $total)); ?>
          </div>
        <?php endif; ?>
      </div>
    </a>
  <?php else: ?>
    test
  <?php endif; ?>
</div>