<?php

/**
 * @file
 * Default theme implementation to basic cart add to cart message.
 *
 * Available variables:
 * @params $order
 */
?>
<div id="modal-overlay">
	<div id="modal-backdrop"></div>
	<div id="modal">
		<div class="product-message-ok message-ok text-center">
			<div class="modal-close">
			</div>
			 <div class="image"></div>
			 <div class="text">
			   <div class="message">
			    <?php print t('Product successfully add to cart'); ?>
			   </div>
			   <div>
			    <div class="continue">
				    <a href="#">
				    	<?php print t('Continue shopping'); ?>
				    </a>
				   </div>
			    <div>
				    <div class="medium primary button">
				    	<a href="/cart/">
				    		<?php print t('Checkout'); ?>
				    	</a>
				    </div>
				   </div>
			   </div>
			 </div>
		</div>
	</div>
</div>
