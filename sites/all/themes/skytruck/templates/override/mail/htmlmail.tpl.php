<?php

/**
 * @file
 * Custom template for HTML Mail
 *
 * The following variables available in this template:
 *
 * $body
 *        The message body text.
 *
 * $module
 *        The first argument to [3]drupal_mail(), which is, by convention,
 *        the machine-readable name of the sending module.
 *
 * $key
 *        The second argument to [4]drupal_mail(), which should give some
 *        indication of why this email is being sent.
 *
 * $message_id
 *        The email message id, which should be equal to
 *        "{$module}_{$key}".
 *
 * $headers
 *        An array of email (name => value) pairs.
 *
 * $from
 *        The configured sender address.
 *
 * $to
 *        The recipient email address.
 *
 * $subject
 *        The message subject line.
 *
 * $body
 *        The formatted message body.
 *
 * $language
 *        The language object for this message.
 *
 * $params
 *        Any module-specific parameters.
 *
 * $template_name
 *        The basename of the active template.
 *
 * $template_path
 *        The relative path to the template directory.
 *
 * $template_url
 *        The absolute URL to the template directory.
 *
 * $theme
 *        The name of the Email theme used to hold template files. If the
 *        [5]Echo module is enabled this theme will also be used to
 *        transform the message body into a fully-themed webpage.
 *
 * $theme_path
 *        The relative path to the selected Email theme directory.
 *
 * $theme_url
 *        The absolute URL to the selected Email theme directory.
 *
 * $debug
 *        TRUE to add some useful debugging info to the bottom of the
 *        message.
 *
 * Other modules may also add or modify theme variables by implementing a
 * MODULENAME_preprocess_htmlmail(&$variables) [6]hook function.
 *
 */
  $template_name = basename(__FILE__);
  $current_path = realpath(NULL);
  $current_len = strlen($current_path);
  $template_path = realpath(dirname(__FILE__));
  if (!strncmp($template_path, $current_path, $current_len)) {
    $template_path = substr($template_path, $current_len + 1);
  }
  $template_url = url($template_path, array('absolute' => TRUE));
?>
<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #f6f6f6; font-size: 14px; line-height: 18px; font-family: Arial;">
  <tbody>
    <tr>
      <td>
        <table cellpadding="0" cellspacing="0" border="0" width="720" align="center">
          <tr>
            <td height="17" style="background: #d7473e; border-bottom: 3px solid #cc3b32;">
              &nbsp;
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="color: #7d7d7d;">
        <table align="center" cellpadding="0" cellspacing="0" border="0" width="720" style="background: #fff;">
          <tr>
            <td style="padding: 20px 0 0;">
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="600" style="color: #333333;">
                <tr>
                  <td>
                    <a href="http://skytruck.ru/">
                      <img src="<?php print 'http://skytruck.ru/' . $template_path. '/images/biglogo.png'; ?>" width="235" />
                    </a>
                  </td>
                  <td width="140" style="text-align: right; line-height: 24px;">
                    +7 (926) 305-80-80<br />
                    +7 (925) 081-32-72<br />
                    <a href="mailto:info@skytruck.ru" style="color: #d7473e;">info@skytruck.ru</a>
                    
                  </td>
                  <td width="215" style="text-align: right;">
                    <img src="<?php print 'http://skytruck.ru/' . $template_path. '/images/main.png'; ?>" width="200" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 0 0 15px;">
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                  <td>
                    <h1 style="color: #d7473e; margin: 0; font-size: 22px; line-height: 26px;">
                      <?php print $subject; ?>
                    </h1>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 0 0 15px; font-size: 14px; line-height: 18px; color: #7d7d7d;">
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                  <td>
                    <?php print $body; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- <tr>
            <td>
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                  <td width="420">
                    &nbsp;
                  </td>
                  <td width="180" style="padding: 0 0 40px; color: #7d7d7d; font-size: 12px; line-height: 18px;">
                    С уважением, <br />
                    команда «SkyTruck»
                  </td>
                </tr>
              </table>
            </td>
          </tr> -->
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table cellpadding="0" cellspacing="0" border="0" width="720" align="center">
          <tr>
            <td style="width: 60px; padding: 25px 0 30px; background: #d7473e; border-top: 7px solid #cc3b32;" >
            </td>
            <td style="width: 245px; padding: 25px 0 30px; background: #3eafc9; border-top: 7px solid #228fa7;" >
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="245" style=" color: #fff; font-size: 12px; line-height: 18px;">
                <tr>
                  <td width="245" style="padding-left: 15px;">
                    Skytruck © <?php  print '2011 - ' . date('Y'); ?><br />
                    +7 (926) 305-80-80
                    Запчасти для китайских грузовиков
                  </td>
                </tr>
              </table>
            </td>
            <td style="padding: 25px 0 30px; background: #d7473e; border-top: 7px solid #cc3b32;" >
              <table align="right" cellpadding="0" cellspacing="0" border="0" width="130" style=" color: #fff; font-size: 12px; line-height: 18px;">
                <tr>
                  <td width="130" style="text-align: right; padding-right: 15px;">
                    <!-- <a href="http://www.youtube.com/user/SkytruckRU"><img src="<?php //print 'http://skytruck.ru/' . $template_path. '/images/yt2.png'; ?>" style="margin: 0 10px 0 0;" /></a><a href="https://twitter.com/skytruckru"><img src="<?php //print 'http://skytruck.ru/' . $template_path. '/images/social_tw.png'; ?>" /></a> -->
                  </td>
                </tr>
              </table>
            </td>
            <td style="width: 60px; padding: 25px 0 30px; background: #d7473e; border-top: 7px solid #cc3b32;" >
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </tbody>
</table>
