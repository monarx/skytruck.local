<?php

/**
 * @file
 * Custom template for sending user password reset messages with HTML Mail
 *
 * The following variables are available in this template:
 *
 *  - $message_id: The email message id, which is 'user_password_reset'
 *  - $module: The sending module, which is 'user'.
 *  - $key: The user email action, which is 'password_reset'.
 *  - $headers: An array of email (name => value) pairs.
 *  - $from: The configured sender address.
 *  - $to: The recipient email address.
 *  - $subject: The message subject line.
 *  - $body: The formatted message body.
 *  - $language: The language object for this message.
 *  - $params: An array containing the following keys:
 *    - account: The user object whose password is being requested, which
 *      contains the following useful properties:
 *      - uid: The user-id number.
 *      - name: The user login name.
 *      - mail: The user email address.  Should be the same as $to.
 *      - theme: The user-chosen theme, or a blank string if unset.
 *      - signature: The user signature block.
 *      - signature_format: The text input filter used to format the signature.
 *      - created: Account creation date, as a unix timestamp.
 *      - access: Account access date, as a unix timestamp.
 *      - login: Account login date, as a unix timestamp.
 *      - status: Integer 0 = disabled; 1 = enabled.
 *      - timezone: User timezone, or NULL if unset.
 *      - language: User language, or blank string if unset.
 *      - picture: Path to user picture, or blank string if unset.
 *      - init: The email address used to initially register this account.
 *      - data: User profile data, as a serialized string.
 *      - roles: Array of roles assigned to this user, as (rid => role_name)
 *        pairs.
 *  - $template_path: The relative path to the template directory.
 *  - $template_url: The absolute url to the template directory.
 *  - $theme: The name of the selected Email theme.
 *  - $theme_path: The relative path to the Email theme directory.
 *  - $theme_url: The absolute url to the Email theme directory.
 */
  $template_name = basename(__FILE__);
  $current_path = realpath(NULL);
  $current_len = strlen($current_path);
  $template_path = realpath(dirname(__FILE__));
  if (!strncmp($template_path, $current_path, $current_len)) {
    $template_path = substr($template_path, $current_len + 1);
  }
  $template_url = url($template_path, array('absolute' => TRUE));
?>
  <table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #fff; font-size: 14px; line-height: 18px; font-family: Arial;">
  <tbody>
    <tr>
      <td>
        <table cellpadding="0" cellspacing="0" border="0" width="720" align="center">
          <tr>
            <td height="17" style="background: #d7473e; border-bottom: 3px solid #cc3b32;">
              &nbsp;
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="color: #7d7d7d;">
        <table align="center" cellpadding="0" cellspacing="0" border="0" width="600">
          <tr>
            <td style="padding: 20px 0;">
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="600" style="color: #333333;">
                <tr>
                  <td>
                    <a href="http://skytruck.ru/">
                      <img src="<?php print 'http://skytruck.ru/' . $template_path. '/images/biglogo.png'; ?>" width="235" />
                    </a>
                  </td>
                  <td width="140" style="text-align: right; line-height: 24px;">
                    +7 (926) 305-80-80<br />
                    +7 (925) 081-32-72<br />
                    <a href="mailto:info@skytruck.ru" style="color: #d7473e;">info@skytruck.ru</a>
                    
                  </td>
                  <td width="215" style="text-align: right;">
                    <img src="<?php print 'http://skytruck.ru/' . $template_path. '/images/main.png'; ?>" width="200" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding: 0 0 30px;">
              <h1 style="width: 300px; color: #d7473e; font-size: 22px; line-height: 26px;">
                <?php print $subject; ?>
              </h1>
            </td>
          </tr>
          <tr>
            <td style="padding: 0 0 15px; font-size: 14px; line-height: 18px; color: #7d7d7d;">
              <?php print $body; ?>
            </td>
          </tr>
          <!-- <tr>
            <td>
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="600">
                <tr>
                  <td width="420">
                    &nbsp;
                  </td>
                  <td width="180" style="padding: 0 0 40px; color: #7d7d7d; font-size: 12px; line-height: 18px;">
                    С уважением, <br />
                    команда «SkyTruck»
                  </td>
                </tr>
              </table>
            </td>
          </tr> -->
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table cellpadding="0" cellspacing="0" border="0" width="720" align="center">
          <tr>
            <td style="width: 60px; padding: 25px 0 30px; background: #d7473e; border-top: 7px solid #cc3b32;" >
            </td>
            <td style="width: 245px; padding: 25px 0 30px; background: #3eafc9; border-top: 7px solid #228fa7;" >
              <table align="center" cellpadding="0" cellspacing="0" border="0" width="245" style=" color: #fff; font-size: 12px; line-height: 18px;">
                <tr>
                  <td width="245" style="padding-left: 15px;">
                    Skytruck © <?php  print '2011 - ' . date('Y'); ?><br />
                    +7 (926) 305-80-80
                    Запчасти для китайских грузовиков
                  </td>
                </tr>
              </table>
            </td>
            <td style="padding: 25px 0 30px; background: #d7473e; border-top: 7px solid #cc3b32;" >
              <table align="right" cellpadding="0" cellspacing="0" border="0" width="130" style=" color: #fff; font-size: 12px; line-height: 18px;">
                <tr>
                  <td width="130" style="text-align: right; padding-right: 15px;">
                    <!-- <a href="http://www.youtube.com/user/SkytruckRU"><img src="<?php //print 'http://skytruck.ru/' . $template_path. '/images/yt2.png'; ?>" style="margin: 0 10px 0 0;" /></a><a href="https://twitter.com/skytruckru"><img src="<?php //print 'http://skytruck.ru/' . $template_path. '/images/social_tw.png'; ?>" /></a> -->
                  </td>
                </tr>
              </table>
            </td>
            <td style="width: 60px; padding: 25px 0 30px; background: #d7473e; border-top: 7px solid #cc3b32;" >
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </tbody>
</table>
