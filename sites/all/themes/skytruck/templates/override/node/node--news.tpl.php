<?php
// $Id: node--news.tpl.php

/**
 * @file
 * Theme implementation to display a node news.
 *
 * Available variables:
 *  @see node.tpl.php
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> row"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php if ($view_mode == 'teaser'): ?>
      <div class="news-teaser">
          <div class="news-date">
            <?php echo format_date($created, 'custom', 'd.m.Y'); ?>
          </div>
          <div class="news-image">
            <?php print render($content['field_image']); ?>
          </div>
          <div class="news-title">
            <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
          </div>
          
          <div class="news-text">
            <?php print render($content); ?>
          </div>
      </div>
    <?php else: ?>
      <div class="news-date">
        <?php echo format_date($created, 'custom', 'd.m.Y'); ?>
      </div>
      <div class="news-body">
        <?php print render($content); ?>
      </div>
    <?php endif; ?>
  </div>
</div>