<?php
// $Id: node--news.tpl.php

/**
 * @file
 * Theme implementation to display a node news.
 *
 * Available variables:
 *  @see node.tpl.php
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <?php if ($view_mode == 'teaser'): ?>
      <div class="row">
        <?php hide($content['links']); ?>
        <?php hide($content['product_reference_price']); ?>
        <div class="image">
          <a href="<?php print $node_url; ?>" target="_blank">
            <?php print render($content['field_image']); ?>
          </a>
        </div>
        <div class="body">
          <div class="title">
            <a href="<?php print $node_url; ?>" target="_blank">
              <?php print $title; ?>
            </a>
          </div>
          <?php print render($content); ?>
          <div class="minimal-price">
            <?php print render($content['product_reference_price']); 
            ?>
          </div>
        </div>
      </div>
    <?php else: ?>
      <div class="row">
        <div class="left">
          <?php print render($content['field_image']); ?>
        </div>
        <div class="right">
          <?php print render($content['body']); ?>
          <?php print render($content['field_youtube']); ?>
        </div>
      </div>
      <div class="entry-content">
        <div class="price-table">
          <?php print render($content['field_availability']); ?>
        </div>
        <?php print render($content); ?>
      </div>
    <?php endif; ?>
  </div>
</div>