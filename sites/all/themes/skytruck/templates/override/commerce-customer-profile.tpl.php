<?php

/**
 * @file
 * Default theme implementation for entities.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    <div class="content"<?php print $content_attributes; ?>>
      <?php if(($commerce_customer_profile->type == 'corporation') && isset($content['field_fio']) && $view_mode != 'customer'): ?>
        <table cellpadding="0" cellspacing="0" width="100%" style="border: 0; border-bottom:1px solid #dcdcdc;">
          <tbody>
            <tr>
              <td style="font-size:14px;color:#838383;padding:20px 15px 0;">
                Тип профиля
              </td>
            </tr>
            <tr>
              <td style="font-size:14px;color:#191919; padding:5px 15px 10px;">
                Юридическое лицо
              </td>
            </tr>
          </tbody>
        </table>
      <?php elseif(($commerce_customer_profile->type == 'individual') && isset($content['field_fio']) && $view_mode != 'customer'): ?>
        <table cellpadding="0" cellspacing="0" width="100%" style="border: 0; border-bottom:1px solid #dcdcdc;">
          <tbody>
            <tr>
              <td style="font-size:14px;color:#838383; padding:20px 15px 0;">
                Тип профиля
              </td>
            </tr>
            <tr>
              <td style="font-size:14px;color:#191919; padding:0px 15px 10px;">
                Физическое лицо
              </td>
            </tr>
          </tbody>
        </table>
      <?php endif; ?>
      <?php if(isset($content['field_fio']) || isset($content['field_shipping_address_postcode'])): ?>
        <?php if($commerce_customer_profile->type != 'shipping'): ?>
          <table class="profile-information" style="width: 100%; font-size: 14px; font-family: Arial; line-height: 18px; margin: 20px 15px 10px;">
        <?php else: ?>
          <table class="profile-information" style="width: 100%; font-size: 14px; font-family: Arial; line-height: 18px; margin: 0 0 -10px;">
        <?php endif; ?>
            <tr><td style="padding: 0;">
                <?php foreach($content as $key => $element): ?>
                  <p style="line-height: 25px; margin: 0">
                    <span style="text-align: left; color: #838383; font-weight: normal;"><?php print $element['#title']; ?>:</span> <span><?php print $element['#items'][0]['value']; ?></span>
                  </p>
                <?php endforeach; ?>
            </td></tr>
          </table>
        <?php /*if($commerce_customer_profile->type != 'shipping'): ?>
          <table class="profile-information" style="width: 100%; font-size: 14px; font-family: Arial; line-height: 18px; margin: 20px 15px;">
        <?php else: ?>
          <table class="profile-information" style="width: 100%; font-size: 14px; font-family: Arial; line-height: 18px; margin: 0;">
        <?php endif;*/ ?>
        <?php /*  <thead>
            <tr>
              <?php foreach($content as $key => $element): ?>
                <?php if (isset($element['#title']) && $key != '#children'):
                      ?>
                  <th style="text-align: left; color: #838383; padding: 10px 15px 10px 0; font-weight: normal;">
                    <?php print $element['#title']; ?>
                  </th>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <?php foreach($content as $key => $element): ?>
                <?php if (isset($element['#items']) && $key != '#children'):
                      ?>
                  <td style="padding: 10px 15px 10px 0;">
                    <?php print $element['#items'][0]['value']; ?>
                  </td>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
          </tbody>
        </table> */?>
      <?php endif; ?>
    </div>
</div>
