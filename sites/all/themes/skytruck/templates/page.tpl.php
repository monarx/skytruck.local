<div class="wrapper">
	<div id="container">		
		<!-- <div class="row">
			<div id="holiday" class="text-center">
				<img src="http://skytruck.ru/sites/all/themes/skytruck/images/holiday/may9.png" style="display: block; margin: 0 auto;" alt="" />
			</div>
		</div> -->
		<div id="primary">
			<div class="row">
				<div class="logo-container">
					<?php if ($logo): ?>
						<?php if (!$is_front) { ?>
							<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
						<?php } else { ?>
							<span id="logo" title="<?php print t('Home'); ?>">
						<?php } ?>
							<img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
						<?php if (!$is_front) { ?>
							</a>
						<?php } else { ?>
							</span>
						<?php } ?>
					<?php endif; ?>
				</div>
				<?php if ($user_menu): ?>
					<nav id="navbar-user" class="navbar">
						<i class="login-ico"></i>
						<a class="toggle" gumby-trigger="#navbar-user > ul" href="#">
							<i class="icon-menu"></i>
						</a>
						<?php print render($user_menu); ?>
					</nav>
				<?php endif; ?>	
				<?php if ($top_menu): ?>
					<nav id="navbar-top" class="navbar column">
						<a class="toggle" gumby-trigger="#navbar-top > ul" href="#">
							<i class="icon-menu"></i>
						</a>
						<?php print render($top_menu); ?>
					</nav>
				<?php endif; ?>
			</div>
		</div>
		<header id="header">
			<?php if ($page['header_before']): ?>
				<section class="header-before">
					<div class="row">
						<?php print render($page['header_before']); ?>
						<div class="two columns">

						</div>
					</div>
				</section>
			<?php endif; ?>

			<?php if ($page['header']): ?>
				<section class="header">
					<div class="row">
						<?php print render($page['header']); ?>

						<div class="brand-logo">
							<?php print $brand_logo; ?>
						</div>
					</div>
				</section>
			<?php endif; ?>

			<section class="header-after">
				<div class="tabs">
					<ul class="tab-nav clearfix">
					    <li class="red active">
					    	<div class="background"></div>
					    	<?php print l(t('Auto brand'), 'catalog'); ?>
					    </li>
					    <li class="blue">
					    	<div class="background"></div>
					    	<?php print l(t('Manufacturer'), ''); ?>
					    </li>
					</ul>

					<?php if ($main_menu): ?>
						<div class="main-menu-wrapper menu-wrapper red tab-content active">
							<div class="row">						
								<nav id="navbar-main" class="navbar navbar-main five columns">
									<a class="toggle" gumby-trigger="#navbar-main > ul" href="#">
										<i class="icon-menu"></i>
									</a>
									<?php print render($main_menu); ?>
								</nav>
								<div class="pull-right">
									<div class="medium primary button">
										<a class="ctools-use-modal ctools-modal-modal-popup-request" href="/modal/entityform/request/nojs">
											<?php echo t('Parts on order'); ?>
										</a>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<div class="manufacturer-menu-wrapper menu-wrapper blue tab-content">
						<div class="row">
							<nav id="navbar-manufacturer" class="navbar navbar-manufacturer five columns">
								<a class="toggle" gumby-trigger="#navbar-main > ul" href="#">
									<i class="icon-menu"></i>
								</a>
								<?php print render($manufacturer_menu); ?>
							</nav>
							<div class="pull-right">
								<div class="medium red button">
									<a class="ctools-use-modal ctools-modal-modal-popup-request" href="/modal/entityform/request/nojs">
										<?php echo t('Parts on order'); ?>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</header>

		<section class="row">
			<div class="system-information">
				<?php if ($messages): ?>
					<div id="messages">
						<?php print $messages; ?>
					</div> <!-- /#messages -->
				<?php endif; ?>

				<?php if ($tabs['#primary']): ?>
					<div class="tabs clearfix">
						<?php print render($tabs); ?>
					</div>
				<?php endif; ?>

				<?php print render($page['help']); ?>

				<?php if ($action_links): ?>
					<ul class="action-links">
						<?php print render($action_links); ?>
					</ul>
				<?php endif; ?>
			</div>
		</section>

		<?php if ($page['before_content']): ?>
			<section class="before-content">
				<?php print render($page['before_content']); ?>
			</section>
		<?php endif; ?>

		<div class="row">
			<?php if (drupal_is_front_page() || !($page['sidebar'])): ?>

				<?php print render($title_prefix); ?>
					<?php if ($title): ?>
						<h1 class="content-title">
							<?php print $title; ?>
						</h1>
					<?php endif; ?>
				<?php print render($title_suffix); ?>
				<div id="content" class="without-sidebar">
					<main>
						<?php print render($page['content']); ?>
					</main>
				</div>
			<?php else: ?>
				<div id="content" class="with-sidebar">
					<main>
						<?php print render($title_prefix); ?>
							<?php if ($title): ?>
								<h1 class="content-title">
									<?php print $title; ?>
								</h1>
							<?php endif; ?>
						<?php print render($title_suffix); ?>
						
						<?php print render($page['content']); ?>
					</main>
				</div>

				<?php if ($page['sidebar']): ?>
					<section id="sidebar">
						<?php print render($page['sidebar']); ?>
					</section>
				<?php endif; ?>
			<?php endif; ?>
		</div>
		<?php if ($page['under_content']): ?>
			<section class="under-content">
				<?php print render($page['under_content']); ?>
			</section>
		<?php endif; ?>
	</div>
</div>
<footer id="footer">
	<div class="row">
		<div class="two columns contacts">
			<div class="content">
				<div class="copy">Skytruck &copy;&nbsp;<?php  print '2011 - ' . date('Y'); ?></div>
				<?php if (theme_get_setting('phone')): ?>
					<div class="phone">
						<?php echo theme_get_setting('phone'); ?>
					</div>
				<?php endif; ?>
				<?php if ($site_slogan) { ?>
					<div class="slogan">
						<?php print $site_slogan; ?>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="two columns">
			<div class="social text-right">
				<?php 
				if (theme_get_setting('youtube'))
					echo l('<i class="youtube-icon"></i>',theme_get_setting('youtube'), array(
						'attributes' => array(
							'target' => '_blank'
						),
						'html' => true)
				);

				if (theme_get_setting('twiter'))
					echo l('<i class="twiter-icon"></i>',theme_get_setting('twiter'), array(
						'attributes' => array(
							'target' => '_blank'
						),
						'html' => true)
				);
				?>
			</div>
		</div>
		<div class="two columns">
			<div class="nexton text-right">
				<a href="http://nexton.ru" title="<?php  print t('Site creation and promotion'); ?>" target="_blank">
					<?php  print t('Site creation and promotion'); ?>
				</a> 
				&mdash; 
				<a href="http://nexton.ru" target="_blank">
					<img alt="<?php  print t('Site creation and promotion'); ?>" src="<?php print base_path() . path_to_theme(); ?>/images/footer/nexton-logo_black.png" style="vertical-align: middle;" />
				</a>
			</div>
		</div>
	</div>
</footer>

<!-- <div id="scroller" class="b-top">
	<div>
	</div>
	<span class="b-top-but">
		<?php //echo t('Upward'); ?>
	</span>
</div> -->
