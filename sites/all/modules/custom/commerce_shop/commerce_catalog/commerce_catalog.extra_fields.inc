<?php

/**
 * @file.
 * Implements Node extrafields.
 */

/**
 * Implements hook_field_extra_fields().
 * @return array
 */
function commerce_catalog_field_extra_fields() {
  return array(
    'node' => array(
      'detail' => array(
        'display' => array(

          'product_reference_sku' => array(
            'label' => t('Commerce product reference: sku list'),
            'description' => t('Commerce product reference sku list.'),
            'callback' => 'commerce_catalog_field_parts_reference_sku',
            'weight' => 0,
          ),

          'product_reference_price' => array(
            'label' => t('Commerce product reference: minimal price'),
            'description' => t('Minimal price for commerce product reference.'),
            'callback' => 'commerce_catalog_field_parts_reference_price',
            'weight' => 0,
          ),

        ),
      ),
    ),
  );
}

/**
 * Implements hook_node_view().
 * @param $node
 * @param $view_mode
 * @param $langcode
 */
function commerce_catalog_node_view($node, $view_mode, $langcode) {
  $extra_fields = field_extra_fields_get_display('node', $node->type, $view_mode);

  //List sku referenced commerce products.
  if (($node->type == 'detail') && isset($extra_fields['product_reference_sku']) && $extra_fields['product_reference_sku']['visible']) {
    $node->content['product_reference_sku'] = array(
      '#markup' =>  commerce_catalog_extra_fields_calculate($node, 'product_reference_sku'),
      '#weight' => $extra_fields['product_reference_sku']['weight'] + 1000,
    );
  }

  //Minimal price referenced commerce products.
  if (($node->type == 'detail') && isset($extra_fields['product_reference_price']) && $extra_fields['product_reference_price']['visible']) {
    $content = commerce_catalog_extra_fields_calculate($node, 'product_reference_price');
    $node->content['product_reference_price'] = array(
      '#markup' => $content,
      '#weight' => $extra_fields['product_reference_price']['weight'] + 1000,
    );
  }
}

/**
 * Extra field views integration callback.
 * See commerce_catalog_field_extra_fields().
 * @param $node
 * @return string
 */
function commerce_catalog_field_parts_reference_sku($node) {
  return commerce_catalog_extra_fields_calculate($node, 'product_reference_sku');
}

/**
 * Extra field views integration callback.
 * See commerce_catalog_field_extra_fields().
 * @param $node
 * @return string
 */
function commerce_catalog_field_parts_reference_price($node) {
  return commerce_catalog_extra_fields_calculate($node, 'product_reference_price');
}


/**
 * Calcualte extra fields value.
 * See commerce_catalog_field_extra_fields().
 * @param $node
 * @param string $key
 * @return mixed|string
 */
function commerce_catalog_extra_fields_calculate($node, $key = 'product_reference_sku') {
  $items = array();
  $price_list = array();
  foreach (field_info_instances('node', $node->type) as $entity_field_name => $entity_field) {
    $field_info = field_info_field($entity_field_name);

    //Skip other fields.
    if ($field_info['type'] != 'commerce_product_reference') {
      continue;
    }

    $wrapper = entity_metadata_wrapper('node', $node);
    $commerce_products = $wrapper->{$entity_field_name}->value();

    foreach($commerce_products as $commerce_product) {
      if (isset($commerce_product->field_sku[LANGUAGE_NONE][0]['value'])) {
        $items[] = $commerce_product->field_sku[LANGUAGE_NONE][0]['value'];
      }
      $price = commerce_product_calculate_sell_price($commerce_product);
      $price_list[] = $price['amount'];
    }

  }

  $minimal_price = (empty($price_list))?0:number_format(min($price_list), 0, '', ' ');

  return ($key == 'product_reference_sku')?
         theme('item_list', array('items' => $items, 'attributes' => array('class' => array('product-reference-sku')))):
         t('Price form @price', array('@price' => $minimal_price));
}