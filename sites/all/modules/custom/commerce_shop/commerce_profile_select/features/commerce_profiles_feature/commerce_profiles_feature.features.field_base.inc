<?php
/**
 * @file
 * commerce_profiles_feature.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function commerce_profiles_feature_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_corporation_company'
  $field_bases['field_corporation_company'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_corporation_company',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_corporation_fio'
  $field_bases['field_corporation_fio'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_corporation_fio',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_corporation_inn'
  $field_bases['field_corporation_inn'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_corporation_inn',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'bigint',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_bigint',
  );

  // Exported field_base: 'field_corporation_kpp'
  $field_bases['field_corporation_kpp'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_corporation_kpp',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'bigint',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_bigint',
  );

  // Exported field_base: 'field_corporation_phone'
  $field_bases['field_corporation_phone'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_corporation_phone',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'phone',
    'settings' => array(
      'country' => 'ru',
    ),
    'translatable' => 0,
    'type' => 'phone',
  );

  // Exported field_base: 'field_individual_fio'
  $field_bases['field_individual_fio'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_individual_fio',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_individual_phone'
  $field_bases['field_individual_phone'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_individual_phone',
    'foreign keys' => array(),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'phone',
    'settings' => array(
      'country' => 'ru',
    ),
    'translatable' => 0,
    'type' => 'phone',
  );

  return $field_bases;
}
