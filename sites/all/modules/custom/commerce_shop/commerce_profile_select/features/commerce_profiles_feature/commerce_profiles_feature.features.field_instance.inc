<?php
/**
 * @file
 * commerce_profiles_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function commerce_profiles_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'commerce_customer_profile-corporation-field_corporation_company'
  $field_instances['commerce_customer_profile-corporation-field_corporation_company'] = array(
    'bundle' => 'corporation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'commerce_customer_profile',
    'field_name' => 'field_corporation_company',
    'label' => 'Организация',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'commerce_customer_profile-corporation-field_corporation_fio'
  $field_instances['commerce_customer_profile-corporation-field_corporation_fio'] = array(
    'bundle' => 'corporation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'commerce_customer_profile',
    'field_name' => 'field_corporation_fio',
    'label' => 'ФИО',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'commerce_customer_profile-corporation-field_corporation_inn'
  $field_instances['commerce_customer_profile-corporation-field_corporation_inn'] = array(
    'bundle' => 'corporation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'bigint',
        'settings' => array(
          'prefix_suffix' => TRUE,
          'thousand_separator' => ',',
        ),
        'type' => 'number_bigint',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'commerce_customer_profile',
    'field_name' => 'field_corporation_inn',
    'label' => 'ИНН',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'bigint',
      'settings' => array(),
      'type' => 'number_bigint',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'commerce_customer_profile-corporation-field_corporation_kpp'
  $field_instances['commerce_customer_profile-corporation-field_corporation_kpp'] = array(
    'bundle' => 'corporation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'bigint',
        'settings' => array(
          'prefix_suffix' => TRUE,
          'thousand_separator' => ',',
        ),
        'type' => 'number_bigint',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'commerce_customer_profile',
    'field_name' => 'field_corporation_kpp',
    'label' => 'КПП',
    'required' => 1,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'bigint',
      'settings' => array(),
      'type' => 'number_bigint',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'commerce_customer_profile-corporation-field_corporation_phone'
  $field_instances['commerce_customer_profile-corporation-field_corporation_phone'] = array(
    'bundle' => 'corporation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'phone',
        'settings' => array(),
        'type' => 'phone',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'commerce_customer_profile',
    'field_name' => 'field_corporation_phone',
    'label' => 'Телефон',
    'required' => 0,
    'settings' => array(
      'ca_phone_parentheses' => 1,
      'ca_phone_separator' => '-',
      'phone_country_code' => 0,
      'phone_default_country_code' => 1,
      'phone_int_max_length' => 15,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'phone',
      'settings' => array(),
      'type' => 'phone_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'commerce_customer_profile-individual-field_individual_fio'
  $field_instances['commerce_customer_profile-individual-field_individual_fio'] = array(
    'bundle' => 'individual',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'commerce_customer_profile',
    'field_name' => 'field_individual_fio',
    'label' => 'ФИО',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'commerce_customer_profile-individual-field_individual_phone'
  $field_instances['commerce_customer_profile-individual-field_individual_phone'] = array(
    'bundle' => 'individual',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'phone',
        'settings' => array(),
        'type' => 'phone',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'commerce_customer_profile',
    'field_name' => 'field_individual_phone',
    'label' => 'Телефон',
    'required' => 0,
    'settings' => array(
      'ca_phone_parentheses' => 1,
      'ca_phone_separator' => '-',
      'phone_country_code' => 0,
      'phone_default_country_code' => 1,
      'phone_int_max_length' => 15,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'phone',
      'settings' => array(),
      'type' => 'phone_textfield',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('ИНН');
  t('КПП');
  t('Организация');
  t('Телефон');
  t('ФИО');

  return $field_instances;
}
