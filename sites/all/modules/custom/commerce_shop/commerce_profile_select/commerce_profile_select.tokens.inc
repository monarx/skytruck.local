 <?php
/**
 * @file Customer profile type tokens.
 */

 /**
  * Implements hook_token_info().
  *
  * @return array
  */
function commerce_profile_select_token_info() {
  // Tokens for orders.
  $tokens = array();

  if (module_exists('customer_profile_type_ui')) {

    $profiles = commerce_customer_profile_types();
    //$result = db_query('SELECT * FROM {customer_profile_type_ui} cpt');

    foreach ($profiles as $customer_profile_type) {
      $tokens['tokens']['commerce-order']['customer-profile-' . $customer_profile_type['type'] . '-values'] = array(
        'name' => t('Commerce profile select: Customer profile ' . $customer_profile_type['name'] .' values'),
        'description' => t('Customer profile values (email format).'),
      );
      $field_instances = field_info_instances('commerce_customer_profile', $customer_profile_type['type']);
      foreach (element_children( $field_instances) as $profile_field) {
        $tokens['tokens']['commerce-order']['customer-profile-' . $customer_profile_type['type'] . '-' . str_replace('_', '-', $profile_field)] = array(
          'name' => t('Commerce profile select: ' .  $customer_profile_type['name'] . ' : ' . $profile_field),
          'description' => t('Commerce profile select: ' .  $customer_profile_type['name'] . ' : ' . $profile_field),
        );
      }
    }
  }

  return $tokens;
}

 /**
  * Implements hook_tokens().
  *
  * @param $type
  * @param $tokens
  * @param array $data
  * @param array $options
  * @return array
  */
function commerce_profile_select_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    
    $order = $data['commerce-order'];
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    if (module_exists('customer_profile_type_ui')) {
      $profiles = commerce_customer_profile_types();
      //$result = db_query('SELECT * FROM {customer_profile_type_ui} cpt');

      foreach ($profiles as $customer_profile_type) {
        $replacements['[commerce-order:customer-profile-' . $customer_profile_type['type'] . '-values]'] = '';
        //All profile values in one token.
        if (isset($order->{'commerce_customer_' . $customer_profile_type['type']}) && !empty($order->{'commerce_customer_' . $customer_profile_type['type']})) {
          $profile = $order_wrapper->{'commerce_customer_' . $customer_profile_type['type']}->value();
          $content = entity_view('commerce_customer_profile', array($profile->profile_id => $profile), 'token');

          $replacements['[commerce-order:customer-profile-' . $customer_profile_type['type'] . '-values]'] = drupal_render($content);
        }

        //Separated profile values.
        $field_instances = field_info_instances('commerce_customer_profile', $customer_profile_type['type']);
        foreach (element_children($field_instances) as $profile_field) {
          $replacements['[' . $type . ':customer-profile-' . $customer_profile_type['type'] . '-' . str_replace('_', '-', $profile_field) . ']'] = (isset($order->{'commerce_customer_' . $customer_profile_type['type']}->{$profile_field}))?$order_wrapper->{'commerce_customer_' . $customer_profile_type['type']}->{$profile_field}->value():'';
        }
      }
    }
    
  }
 
  return $replacements;
}
