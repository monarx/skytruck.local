<?php
/**
 * @file commerce_search_autocomplete.module
 */

/**
 * Implements hook_menu().
 * @return mixed
 */
function commerce_search_autocomplete_menu() {
  $items['commerce_search_autocomplete/search/autocomplete'] = array(
    'page callback' => 'commerce_search_autocomplete_view',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Autocomplete search query.
 * @param string $text
 */
function commerce_search_autocomplete_view($text = '') {
  $values = array();

  if ($text) {
    $query = db_select('commerce_product', 'cp');
    $query->innerJoin('field_data_field_sku', 'sku', 'cp.product_id = sku.entity_id');
    $query->fields('cp', array('sku', 'title'));
    $query->fields('sku', array('field_sku_value'));
    $query->condition('cp.type', 'part');
    $query->condition('sku.bundle', 'part');
    $query->condition('cp.status', 1);
    $query->condition(db_or()->condition('cp.title', '%' . $text . '%', 'LIKE')->condition('sku.field_sku_value', '%' . $text . '%', 'LIKE'));
    $query->range(0, 10);

    $result = $query->execute();

    foreach ($result as $key => $record) {
      $values[] = array(
        'label' => $record->title . ' ' . $record->field_sku_value,
      );
    }
  }

  drupal_json_output($values);
}

/**
 * Implements hook_block_info().
 * @return mixed
 */
function commerce_search_autocomplete_block_info() {
  $blocks['parts-search'] = array(
    'info' => 'Parts search'
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 * @param string $delta
 * @return array
 */
function commerce_search_autocomplete_block_view($delta = '') {
  $block = array();

  if ($delta == 'parts-search') {
    $form = drupal_get_form('commerce_search_autocomplete_form');
    $block['content'] = drupal_render($form) . '<div class="extended-parts-search">' . l(t('Extended parts search'), 'search', array('query' => array('parts_search_brand' => 'All'))) . '<div class="parts-catalogue">' . l(t('Electronic spare parts catalogue'), 'http://cat.skytruck.ru/gruzovye_avtomobili.html', array('attributes' => array('target' => '_blank'))) . '</div>' . '</div>';
  }

  return $block;
}

/**
 * Parts search form.
 * @return array
 */
function commerce_search_autocomplete_form() {
  $form = array(
    '#validate' => array('commerce_search_autocomplete_form_validate'),
  );
  $form['parts_search_autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#default_value' => '',
    '#attributes' => array(
      'placeholder' => t('Sku or title'),
    ),
    '#suffix' => '<div class="parts-search-example">' . t('For example <span>!example</span>', array('!example' => 'DZ93259820130')) . ' ' . t('or') . ' <span>' . t('oil filter') . '</span>' . '</div>',
  );
  $form['parts_search_submit'] = array(
    '#type' => 'submit',
    '#value' => '',
  );
  return $form;
}

/**
 * Parts search form validate.
 * @param $form
 * @param $form_state
 */
function commerce_search_autocomplete_form_validate($form, &$form_state) {
  $form_state['redirect'] = array(
    'search',
      array('query' => array('parts_search_text' => $form_state['values']['parts_search_autocomplete'], 'parts_search_brand' => 'All')),
  );
}