<?php

/**
 * @file
 * commerce_parts.feeds_importer_default.inc
 */

/**
 * hook_feeds_importer_default()
 * Define default importer.
 */
function commerce_replacements_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'replacements_import';
  $feeds_importer->config = array(
    'name' => 'Replacements import',
    'description' => 'Import cross(replacement).',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsECKprocessor',
      'config' => array(
        'entity_type' => 'replacement',
        'bundle' => 'replacement',
        'expire' => -1,
        'author' => 0,
        'authorize' => TRUE,
        'mappings' => array(
          0 => array(
            'source' => 'original',
            'target' => 'original',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'replacement',
            'target' => 'replacement',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'full_html',
        'skip_hash_check' => 1,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );

  $export['replacements_import'] = $feeds_importer;
  return $export;
}