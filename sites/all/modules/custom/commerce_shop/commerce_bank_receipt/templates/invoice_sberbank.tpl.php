<style id="sber_pd4_11428_Styles">
<!--table
	{mso-displayed-decimal-separator:"\,";
	mso-displayed-thousand-separator:" ";}
.xl1511428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6511428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6611428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6711428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Clarendon Extended", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6811428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl6911428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Wingdings;
	mso-generic-font-family:auto;
	mso-font-charset:2;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7011428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:12.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7111428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7211428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7311428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7411428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7511428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7611428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Clarendon Extended", serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7711428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7811428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl7911428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8011428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8111428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8211428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8311428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8411428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8511428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8611428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8711428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:9.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8811428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl8911428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:none;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9011428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9111428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9211428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9311428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9411428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9511428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9611428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9711428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9811428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl9911428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10011428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl10111428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:normal;}
.xl10211428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10311428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.0pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:1.0pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10411428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10511428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:1.0pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10611428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10711428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:1.5pt solid windowtext;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10811428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl10911428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11011428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11111428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11211428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11311428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11411428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:.5pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11511428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11611428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11711428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:justify;
	border-top:1.0pt solid windowtext;
	border-right:1.5pt solid windowtext;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11811428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:7.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:left;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl11911428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:none;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12011428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:1.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12111428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:8.0pt;
	font-weight:700;
	font-style:italic;
	text-decoration:none;
	font-family:"Times New Roman", serif;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:right;
	vertical-align:bottom;
	border-top:1.5pt solid windowtext;
	border-right:none;
	border-bottom:none;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
.xl12211428
	{padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Arial Cyr";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:"\@";
	text-align:center;
	vertical-align:bottom;
	border-top:none;
	border-right:1.5pt solid windowtext;
	border-bottom:.5pt solid windowtext;
	border-left:none;
	mso-background-source:auto;
	mso-pattern:auto;
	white-space:nowrap;}
-->
</style>

<?
//print_r($order);
?>

<table border=0 cellpadding=0 cellspacing=0 width=832 style='border-collapse:
 collapse;table-layout:fixed;width:640pt'>
 <col width=13 span=64 style='width:10pt'>

 
 <? for($i=1;$i<=2;$i++){ ?>
  <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td colspan=15 class=xl10711428 style='border-right:1.5pt solid black'><?=($i==1)? 'Извещение':'Квитанция' ?></td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td class=xl7111428>&nbsp;</td>
  <td colspan=28 class=xl12111428><?=($i==1)? 'Форма № ПД-4':'' ?></td>
  <td class=xl7211428>&nbsp;</td>
 </tr>
 <tr height=17 style='height:12.9pt'>
  <td height=17 class=xl1511428 style='height:12.9pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=48 class=xl11011428 style='border-right:1.5pt solid black'><span
  style='mso-spacerun:yes;text-align:center'><?=variable_get('uc_noncash_name1','') ?>, <?=variable_get('uc_noncash_adress','') ?></span></td>
 </tr>
 <tr height=17 style='height:12.9pt'>
  <td height=17 class=xl1511428 style='height:12.9pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=48 class=xl11211428 style='border-right:1.5pt solid black'>(наименование
  получателя платежа)</td>
 </tr>
 <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=18 class=xl9311428><span style='mso-spacerun:yes'><?=variable_get('uc_noncash_inn','') ?></span></td>
  <td colspan=2 class=xl6511428></td>
  <td colspan=28 class=xl9311428 style='border-right:1.5pt solid black'><span
  style='mso-spacerun:yes'><?=variable_get('uc_noncash_racc','') ?></span></td>
 </tr>
 <tr height=16 style='mso-height-source:userset;height:12.75pt'>
  <td height=16 class=xl1511428 style='height:12.75pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=18 class=xl9611428>(ИНН получателя платежа)</td>
  <td colspan=2 class=xl6511428></td>
  <td colspan=28 class=xl9611428 style='border-right:1.5pt solid black'>( номер
  счета получателя платежа)</td>
 </tr>
 <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=32 class=xl9311428><span style='mso-spacerun:yes'><?=variable_get('uc_noncash_bank','') ?></span></td>
  <td colspan=4 class=xl10411428>БИК</td>
  <td colspan=12 class=xl9311428 style='border-right:1.5pt solid black'><span
  style='mso-spacerun:yes'><?=variable_get('uc_noncash_bik','') ?></span></td>
 </tr>
 <tr height=17 style='height:12.9pt'>
  <td height=17 class=xl1511428 style='height:12.9pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=32 class=xl9611428><span
  style='mso-spacerun:yes'> </span>(наименование банка получателя платежа)</td>
  <td colspan=16 class=xl6511428 style='border-right:1.5pt solid black'></td>
 </tr>
 <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=23 class=xl9911428>Номер кор./сч. банка получателя платежа</td>
  <td colspan=25 class=xl9311428 style='border-right:1.5pt solid black'><span
  style='mso-spacerun:yes'><?=variable_get('uc_noncash_koracc','') ?></span></td>
 </tr>
 <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=32 class=xl10011428 width=416 style='border-left:none;width:320pt'><span
  style='mso-spacerun:yes'>Оплата заказа №<?=$order->order_id ?> в интернет магазине "Buysd.ru" </span></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td colspan=14 class=xl10211428 style='border-right:1.5pt solid black'><span
  style='mso-spacerun:yes'> </span></td>
 </tr>
 <tr height=16 style='mso-height-source:userset;height:12.75pt'>
  <td height=16 class=xl1511428 style='height:12.75pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=30 class=xl9611428>(наименование платежа)<span
  style='mso-spacerun:yes'> </span></td>
  <td colspan=18 class=xl9711428 style='border-right:1.5pt solid black'>(номер
  лицевого счета (код) плательщика)</td>
 </tr>
 <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=10 class=xl6611428>Ф.И.О. плательщика:</td>
  <td colspan=38 class=xl9311428 style='border-right:1.5pt solid black'><span
  style='mso-spacerun:yes'><?=$order->delivery_last_name ?> </span></td>
 </tr>
 <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td colspan=10 class=xl6611428>Адрес плательщика:</td>
  <td colspan=38 class=xl9311428 style='border-right:1.5pt solid black'><span
  style='mso-spacerun:yes'> <?=$order->delivery_adress ?></span></td>
 </tr>
 <tr height=17 style='height:12.9pt'>
  <td height=17 class=xl1511428 style='height:12.9pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl7411428 colspan=8><span style='mso-spacerun:yes'></span>Сумма
  платежа:</td>
  <td colspan=5 class=xl9211428><span style='mso-spacerun:yes'><?=$order->subtotal_total ?> </span></td>
  <td class=xl7411428 colspan=3>руб.</td>
  <td colspan=3 class=xl9511428><span style='mso-spacerun:yes'> 00 </span></td>
  <td class=xl7411428 colspan=2>коп.</td>
  <td class=xl1511428></td>
  <td class=xl7411428 colspan=10>Сумма платы за услуги:</td>
  <td class=xl1511428></td>
  <td colspan=5 class=xl9211428><span style='mso-spacerun:yes'> <?=$order->subtotal_total ?> </span></td>
  <td class=xl7411428 colspan=3>руб.</td>
  <td colspan=3 class=xl9511428><span style='mso-spacerun:yes'> 00 </span></td>
  <td class=xl7411428 colspan=2>коп.</td>
  <td class=xl7511428>&nbsp;</td>
 </tr>
 <tr height=16 style='mso-height-source:userset;height:12.75pt'>
  <td height=16 class=xl1511428 style='height:12.75pt'></td>
  <td class=xl7311428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7411428 colspan=3>Итого</td>
  <td colspan=8 class=xl9211428><span style='mso-spacerun:yes'> <?=$order->subtotal_total ?> </span></td>
  <td class=xl7411428 colspan=3>руб.</td>
  <td colspan=3 class=xl9211428><span style='mso-spacerun:yes'> 00 </span></td>
  <td class=xl7411428 colspan=2>коп.</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7411428>“</td>
  <td colspan=3 class=xl9211428><span style='mso-spacerun:yes'><?=date('d')?></span></td>
  <td class=xl7411428>”</td>
  <td colspan=12 class=xl9211428><span style='mso-spacerun:yes'><?=rusmonth(date('m'))?></span></td>
  <td colspan=2 class=xl8711428>20</td>
  <td colspan=2 class=xl8811428><span style='mso-spacerun:yes'><?=date('y')?> </span></td>
  <td class=xl7411428>г.</td>
  <td class=xl1511428></td>
  <td class=xl1511428></td>
  <td class=xl7511428>&nbsp;</td>
 </tr>
 <tr height=17 style='height:12.9pt'>
  <td height=17 class=xl1511428 style='height:12.9pt'></td>
  <td colspan=15 class=xl8911428 style='border-right:1.5pt solid black'>Кассир</td>
  <td colspan=48 class=xl6711428 style='border-right:1.5pt solid black'>С
  условиями приема указанной в платежном документе суммы, в т.ч. с суммой
  взимаемой платы за услуги банка<span style='mso-spacerun:yes'> </span></td>
 </tr>
 <tr height=18 style='height:13.6pt'>
  <td height=18 class=xl1511428 style='height:13.6pt'></td>
  <td class=xl7711428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl8011428>&nbsp;</td>
  <td colspan=9 class=xl11811428>ознакомлен и согласен.</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7811428>&nbsp;</td>
  <td class=xl7911428 colspan=8>Подпись плательщика</td>
  <td class=xl7811428>&nbsp;</td>
  <td colspan=22 class=xl11911428 style='border-right:1.5pt solid black'>&nbsp;</td>
 </tr>
 

 <? } ?>
 

 
 

</table>
<br />


