<html>
<head>
<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
<title>Счет на оплату</title>
<style>
				input[type="button"] {
					padding: 5px 10px;
				}
        body { width: 210mm; margin-left: auto; margin-right: auto; border: 1px #efefef solid; font-size: 11pt;}
        table.invoice_bank_rekv { border-collapse: collapse; border: 1px solid; }
        table.invoice_bank_rekv > tbody > tr > td, table.invoice_bank_rekv > tr > td { border: 1px solid; }
        .view-id-invoice_receipt_line_items  table { border: 1px solid; border-collapse: collapse;  width: 100%;}
        .view-id-invoice_receipt_line_items  table td, .view-id-invoice_receipt_line_items  table th { border: 1px solid;}
        
        .view-invoice-receipt-line-items th {
          white-space: nowrap;
        }
        td.views-field-commerce-unit-price, td.views-field-commerce-total{
          white-space: nowrap;
        }
        #signs-block {position:relative;padding-bottom:125px}
         
        #sign-boss {padding:70px 0 36px;}
        #img-sign-boss {
          left: 453px;
          position: absolute;
          top: -38px;
        }
        
        #sign-bugalter {padding:32px 0 0px;}
        #img-sign-bugalter {
          left: 426px;
          position: absolute;
          top: 36px;
        }

        #img-stamp {
          left: 426px;
          position: absolute;
          top: -34px;
        }
        #img-stamp img {}
        
        #signs-block img {width:150px;}
		  
		  .field-label-inline {float: left; padding-right: 15px;}
		  .field-label-inline .field-label {float: left;}
		  .field-label-inline .field-items {font-weight: normal; float:left;}
      .contextual-links-wrapper {display: none;}
      @media print {
        #receipt-buttons {display: none;}
      }
    </style>
</head>	 
<body>	 
<? 
$imgpath = '/'.drupal_get_path('module', 'uc_noncash'). 'template/img';
$order = new Stdclass();
?>
<div id="receipt-buttons">
  <input type="button" value="Распечатать" onclick="window.print();" />
  <input type="button" value="Закрыть" onclick="window.close();" />
</div>
<div style="width:100%; height:30px;">&nbsp;</div>

<table width="100%">
  <tr>
    <td colspan="2" style="text-align:center">
            <div>Внимание! Оплата данного счета означает согласие с условиями поставки товара. Уведомление об оплате<br/>
обязательно, в противном случае не гарантируется наличие товара на складе. Товар отгружается по факту<br/>
прихода денег на р/с Поставщика.</div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div style="text-align:center;  font-weight:bold;">
            </div>

        </td>
    </tr>
</table>


<table width="100%" cellpadding="2" cellspacing="2" class="invoice_bank_rekv">
    <tr>
        <td colspan="2" rowspan="2" style="min-height:13mm; width: 105mm;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="height: 13mm;">
                <tr>
                    <td valign="top">

                        <div>[commerce-bank-receipt:bank-name]</div>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" style="height: 3mm;">
                        <div style="font-size:10pt;">Банк получателя        </div>
                    </td>
                </tr>

            </table>
        </td>
        <td style="min-height:7mm;height:auto; width: 25mm;">
            <div>БИK</div>
        </td>
        <td rowspan="2" style="vertical-align: top; width: 60mm;">
            <div style=" height: 7mm; line-height: 7mm; vertical-align: middle;">[commerce-bank-receipt:bank-bik]</div>
            <div>[commerce-bank-receipt:bank-corr]</div>

        </td>
    </tr>
    <tr>
        <td style="width: 25mm;">
            <div>Сч. №</div>
        </td>
    </tr>
    <tr>

        <td style="min-height:6mm; height:auto; width: 50mm;">
            <div>ИНН [commerce-bank-receipt:payee-inn]</div>
        </td>
        <td style="min-height:6mm; height:auto; width: 55mm;">
            <div>КПП [commerce-bank-receipt:payee-kpp]</div>
        </td>
        <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 25mm;">
            <div>Сч. №</div>

        </td>
        <td rowspan="2" style="min-height:19mm; height:auto; vertical-align: top; width: 60mm;">
            <div>[commerce-bank-receipt:payee-account]</div>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="min-height:13mm; height:auto;">

            <table border="0" cellpadding="0" cellspacing="0" style="height: 13mm; width: 105mm;">

                <tr>
                    <td valign="top">
                        <div>[commerce-bank-receipt:payee-name]</div>
                    </td>
                </tr>
                <tr>
                    <td valign="bottom" style="height: 3mm;">
                        <div style="font-size: 10pt;">Получатель</div>

                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<br/>

<div style="font-weight: bold; font-size: 16pt; padding-left:5px;">
    Счет № [commerce-order:order-number] от [date:custom:d.m.Y]</div>
<div style="padding-left: 5px;">Счет действителен в течении 5 рабочих дней.</div>
<br/>

<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>

<table width="100%">
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Поставщик:    </div>
        </td>
        <td>
            <div style="font-weight:bold;  padding-left:2px;">
                ИНН [commerce-bank-receipt:payee-inn], КПП [commerce-bank-receipt:payee-kpp], [commerce-bank-receipt:payee-name], [commerce-bank-receipt:payee-address] </div>
        </td>
    </tr>
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Грузоотправитель:</div>
        </td>
        <td>
            <div style="font-weight:bold;  padding-left:2px;">

               ИНН [commerce-bank-receipt:shipper-inn], КПП [commerce-bank-receipt:shipper-kpp], [commerce-bank-receipt:shipper-name], [commerce-bank-receipt:shipper-address] </div>
        </td>
    </tr>    
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Покупатель:    </div>
        </td>
        <td>

            <div style="font-weight:bold;  padding-left:2px; clear: both">
               [commerce-order:customer-profile-bank-receipt] </div>
        </td>
    </tr>
    <tr>
        <td style="width: 30mm;">
            <div style=" padding-left:2px;">Грузополучатель:    </div>
        </td>
        <td>

            <div style="font-weight:bold;  padding-left:2px; clear: both">
               [commerce-order:customer-profile-bank-receipt] </div>
        </td>
    </tr>    
</table>
<div style="width:100%; height:20px;">&nbsp;</div>
	[commerce-bank-receipt:order-lines-invoice]

<table border="0" width="100%" cellpadding="1" cellspacing="1">

    
    
   <tr>
        <td colspan=2 style="width:27mm; font-weight:bold;  text-align:right;padding-top: 20px;">Итого:</td>
        <td style="width:27mm; font-weight:bold;  text-align:right;padding-top: 20px;">[commerce-order:commerce_order_total]</td>
    </tr>
    <tr>
        <td colspan=2 style="font-weight:bold;  text-align:right;">В том числе НДС [commerce-bank-receipt:payee-nds]%:</td>
        <td style="width:27mm; font-weight:bold;  text-align:right;">[commerce-bank-receipt:order-nds-amount]</td>
   </tr>
    <tr>
        <td colspan=2 style="font-weight:bold;  text-align:right;">Всего к оплате:</td>
        <td style="width:27mm; font-weight:bold;  text-align:right;">[commerce-order:commerce_order_total]</td>
   </tr>   
</table>

<br />
<div>
Всего наименований [commerce-order:order-lines-count], на сумму [commerce-order:commerce_order_total]<br />
<span style="font-weight:bold;">[commerce-bank-receipt:order-str-amount]</span>
</div>
<br />
<div style="background-color:#000000; width:100%; font-size:1px; height:2px;">&nbsp;</div>
<div style="width:100%; height:20px;">&nbsp;</div>
<div id="signs-block">

<table style="width: 100%;">
<tr>
<td>Руководитель</td>
<td style="border-bottom: 1px solid;text-align: center;">Генеральный директор</td>
<td style="border-bottom: 1px solid; width: 50mm;">&nbsp;</td>
<td style="border-bottom: 1px solid;text-align: center;width: 50mm;">Бардаков А. А.</td>
</tr>
<tr>
<td></td>
<td style="text-align: center; font-size: 10px;">должность</td>
<td style="text-align: center; font-size: 10px;">подпись</td>
<td style="text-align: center; font-size: 10px;">расшифровка подписи</td>
</tr>
</table>
<div style="width:100%; height:20px;">&nbsp;</div>
<table style="width: 100%;">
<tr>
<td>Главный (старший) бухгалтер</td>
<td style="border-bottom: 1px solid;text-align: center;"></td>
<td style="border-bottom: 1px solid; width: 50mm;">&nbsp;</td>
<td style="border-bottom: 1px solid;text-align: center;width: 50mm;">Бушулян Ю. Б.</td>
</tr>
<tr>
<td></td>
<td style="text-align: center; font-size: 10px;"></td>
<td style="text-align: center; font-size: 10px;">подпись</td>
<td style="text-align: center; font-size: 10px;">расшифровка подписи</td>
</tr>
</table>

<div id="img-sign-boss" ><img src="/sites/all/modules/custom/commerce_bank_receipt/templates/img/sign-boss.png"></div>
<div id="img-sign-bugalter"><img src="/sites/all/modules/custom/commerce_bank_receipt/templates/img/sign-bugalter.png"></div>
<div id="img-stamp"><img src="/sites/all/modules/custom/commerce_bank_receipt/templates/img/stamp.png"></div>
</div>
<div style="width:800px;text-align:left;font-size:10pt;">Примечание: внутренний номер №[commerce-order:order-number]</div>
<br /><br />
</body>
</html>

