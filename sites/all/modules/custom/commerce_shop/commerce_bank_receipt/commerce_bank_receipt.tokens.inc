<?php

/**
 * Implements hook_token_info().
 * Todo: add bank receipt button.
 */
function commerce_bank_receipt_token_info() {  
  $type = array(
    'name' => t('Commerce bank receipt'),
    'description' => t('Tokens related to bank invoice receipt payment.'),
  );
  
  $tokens = array();  

  /**
   * Bank settings tokens
   */
  $tokens['bank-name'] = array(
    'name' => t('Commerce bank receipt: Bank name'),
    'description' => t('Official name of the bank.'),
  );      
  
  $tokens['bank-mfo'] = array(
    'name' => t('Commerce bank receipt: Bank MFO'),
    'description' => t('MFO of the bank (numeric).'),
  );

  $tokens['bank-bik'] = array(
    'name' => t('Commerce bank receipt: Bank BIK'),
    'description' => t('Bank identification code.'),
  );    
  
  $tokens['bank-corr'] = array(
    'name' => t('Commerce bank receipt: Bank correspondent account'),
    'description' => t('Bank correspondent account.'),
  );
  
  $tokens['payee-name'] = array(
    'name' => t('Commerce bank receipt: Payee name'),
    'description' => t('Official name of the payee.'),
  );
  
  $tokens['payee-address'] = array(
    'name' => t('Commerce bank receipt: Payee address'),
    'description' => t('Official address of the payee.'),
  );
  
  $tokens['payee-inn'] = array(
    'name' => t('Commerce bank receipt: Payee INN'),
    'description' => t('Tax identification number of the payee.'),
  );
  
  $tokens['payee-kpp'] = array(
    'name' => t('Commerce bank receipt: Payee KPP'),
    'description' => t('Payee KPP.'),
  );
  
  $tokens['payee-account'] = array(
    'name' => t('Commerce bank receipt: Payee account'),
    'description' => t('Bank account of the payee.'),
  );
  
  $tokens['payee-nds'] = array(
    'name' => t('Commerce bank receipt: Payee NDS'),
    'description' => t('NDS in percent.'),
  );  

  //Shipper tokens
  $tokens['shipper-name'] = array(
    'name' => t('Commerce bank receipt: Shipper name'),
    'description' => t('Official name of the shipper.'),
  );
  
  $tokens['shipper-address'] = array(
    'name' => t('Commerce bank receipt: Shipper address'),
    'description' => t('Official address of the shipper.'),
  );
  
  $tokens['shipper-inn'] = array(
    'name' => t('Commerce bank receipt: Shipper INN'),
    'description' => t('Tax identification number of the shipper.'),
  );
  
  $tokens['shipper-kpp'] = array(
    'name' => t('Commerce bank receipt: Shipper KPP'),
    'description' => t('Shipper KPP.'),
  );
  
  /**
   * Order information tokens
   */
  $tokens['order-lines-invoice'] = array(
    'name' => t('Commerce bank receipt: Order lines without discount', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Commerce bank receipt: Order lines without discount.'),
  );  
  
  $tokens['order-number'] = array(
   'name' => t('Commerce bank receipt: Order number'),
   'description' => t('Commerce bank receipt: Order number.'),
  );  
  
  $tokens['order-nds-amount'] = array(
    'name' => t('Commerce bank receipt: Order nds amount'),
    'description' => t('Order nds amount.'),
  );
  
  $tokens['order-str-amount'] = array(
    'name' => t('Commerce bank receipt: Order str amount'),
    'description' => t('Order str amount.'),
  );
  
  return array(
    'types' => array('commerce-bank-receipt' => $type),
    'tokens' => array('commerce-bank-receipt' => $tokens),
  );
}

/**
 * Implements hook_tokens().
 */
function commerce_bank_receipt_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
 
  if ($type == 'commerce-bank-receipt') {
    $order = $data['commerce-bank-receipt'];
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

    $currency_default = commerce_default_currency();
    $currency_convert = commerce_bank_receipt_get_settings('currency');
    $amount = $order->commerce_order_total['und'][0]['amount'];
  
    $currency_code = $order_wrapper->commerce_order_total->currency_code->value();
  
    if ($currency_default != $currency_convert) {
      $amount_converted = commerce_currency_convert($amount, $currency_default, $currency_convert);
    }
    else {
      $amount_converted = $amount;
    }
 
    foreach ($tokens as $name => $original) {
      switch ($name) {
        /**
         * Bank settings tokens.
         */
        case 'bank-name':
          $replacements[$original] = commerce_bank_receipt_get_settings('bank_name'); 
        break;                
        
        case 'bank-mfo':
          $replacements[$original] = commerce_bank_receipt_get_settings('bank_mfo');
        break;
        
        case 'bank-bik':
          $replacements[$original] = commerce_bank_receipt_get_settings('bank_bik');
        break;  
        
        case 'bank-corr':
          $replacements[$original] = commerce_bank_receipt_get_settings('bank_corr');
        break;
        
        //Payee
        case 'payee-name':
          $replacements[$original] = commerce_bank_receipt_get_settings('payee_name');
        break;
        
        case 'payee-address':
          $replacements[$original] = commerce_bank_receipt_get_settings('payee_address');
        break;  
        
        case 'payee-inn':
          $replacements[$original] = commerce_bank_receipt_get_settings('payee_inn');
        break;
        
        case 'payee-kpp':
          $replacements[$original] = commerce_bank_receipt_get_settings('payee_kpp');
        break;          
        
        case 'payee-account':
          $replacements[$original] = commerce_bank_receipt_get_settings('payee_account');
        break;
        
        case 'payee-nds':
          $replacements[$original] = commerce_bank_receipt_get_settings('payee_nds');
        break;
        
        //Shipper
        case 'shipper-name':
          $replacements[$original] = commerce_bank_receipt_get_settings('shipper_name');
        break;
        
        case 'shipper-address':
          $replacements[$original] = commerce_bank_receipt_get_settings('shipper_address');
        break;  
        
        case 'shipper-inn':
          $replacements[$original] = commerce_bank_receipt_get_settings('shipper_inn');
        break;
        
        case 'shipper-kpp':
          $replacements[$original] = commerce_bank_receipt_get_settings('shipper_kpp');
        break;  
        
        /**
         * Order information 
         */
        case 'order-number':
          $replacements[$original] = $order->number;
        break;
        
        case 'order-lines-invoice':
          $total = 0;
          $line_items_ids = array();
          foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
            $cloned_line_item = clone($line_item_wrapper->value());
            // If the line item is a product line item...
            if (in_array($cloned_line_item->type, commerce_product_line_item_types())) {
              $line_items_ids[] = $cloned_line_item->line_item_id;
            }  
          }
          $total = $order_wrapper->commerce_order_total->amount->value();
          $replacements[$original] = commerce_embed_view('invoice_receipt_line_items', 'dafault', array(implode(',', $line_items_ids)));
        break;  
        
        case 'order-nds-amount':
          $total = $order_wrapper->commerce_order_total->amount->value();
          $replacements[$original] = commerce_currency_format(($total * commerce_bank_receipt_get_settings('payee_nds') / 118), $currency_code);
        break;      
        
        case 'order-str-amount':
          $total = $order_wrapper->commerce_order_total->amount->value();
          $replacements[$original] = mb_ucfirst(num2str($total / 100));
        break;
      }
    }   
  }
  
  return $replacements;
}