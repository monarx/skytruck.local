<?php
/**
 * @file
 * Builds placeholder replacement tokens for order.
 * List of tokens:
 * -
 */

/**
 * Implements hook_token_info().
 */
function commerce_custom_token_info() {
  $type = array();
  $type[] = array(
    'name' => t('Orders', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Additional order tokens'),
    'needs-data' => 'commerce-order',
  );

  // Tokens for orders.
  $order = array();
 
  $order['order-lines-count'] = array(
    'name' => t('Commerce order tokens: Order lines count', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Order line items count.'),
  ); 
  
  $order['order-line-items-email'] = array(
    'name' => t('Commerce order tokens: Order line items for email.', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Order line items for email.'),
  );  

  $order['order-payment-method'] = array(
    'name' => t('Commerce order tokens: Order payment method', array(), array('context' => 'a drupal commerce order')),
    'description' => t('Order payment method for email.'),
  );  
  
  return array(
    'types' => array('commerce-order' => $type),
    'tokens' => array('commerce-order' => $order),
  );  
}

/**
 * Implements hook_tokens().
 */
function commerce_custom_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $url_options = array('absolute' => TRUE);

  if (isset($options['language'])) {
   $url_options['language'] = $options['language'];
   $language_code = $options['language']->language;
  }
  else {
   $language_code = NULL;
  }

  $sanitize = !empty($options['sanitize']);

  $replacements = array();

  if ($type == 'commerce-order' && !empty($data['commerce-order'])) {
    $order = $data['commerce-order'];
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'order-lines-count':
          $total = 0;
          $line_items_ids = array();
          foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
            $cloned_line_item = clone($line_item_wrapper->value());

            // If the line item is a product line item...
            if (in_array($cloned_line_item->type, commerce_product_line_item_types())) {
              $line_items_ids[] = $cloned_line_item->line_item_id;
            }  
          }        

          $replacements[$original] = count($line_items_ids);
        break;      
        // Simple key values on the order.
        case 'order-line-items-email':
          $replacements[$original] = views_embed_view('order_line_items', 'default', $order->order_id);
        break;
        case 'order-payment-method':
          $payment_selected = '';
          if (!empty($order->data['payment_method'])) {
            list($selected_method_id, ) = explode('|', $order->data['payment_method']);
            $payment_selected = commerce_payment_method_get_title('short_title', $selected_method_id);
          }
          $replacements[$original] = $payment_selected;          
        break;
      }
    }
  }

  return $replacements;
}
