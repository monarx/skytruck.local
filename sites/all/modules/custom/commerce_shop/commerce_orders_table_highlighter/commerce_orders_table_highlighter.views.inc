<?php

/**
 * Implements hook_views_plugins().
 */
function commerce_orders_table_highlighter_views_plugins() {
  return array(
    'style' => array(
      'commerce_orders_table_highlighter' => array(
        'title' => t('Colored order table'),
        'help' => t('Order table row color depends on the order status.'),
        'handler' => 'commerce_orders_table_highlighter_plugin_style',
        'parent' => 'table',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'theme' => 'views_view_table',
        'even empty' => TRUE,
      )
    )
  );
}
