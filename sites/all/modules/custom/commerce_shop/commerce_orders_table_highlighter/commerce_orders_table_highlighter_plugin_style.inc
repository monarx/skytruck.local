<?php

/**
 * @file
 * commerce_orders_table_highlighter_plugin_style.inc
 * Class commerce_orders_table_highlighter_plugin_style
 */
class commerce_orders_table_highlighter_plugin_style extends views_plugin_style_table {
  function options_form(&$form, &$form_values) {
    parent::options_form($form, $form_values);

    $form['views_table_highlighter'] = array(
      '#type' => 'value',
      '#value' => 1,
    );
  }
}

