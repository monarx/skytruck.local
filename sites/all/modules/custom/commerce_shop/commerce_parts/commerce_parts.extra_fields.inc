<?php

/**
 * @file.
 * Implements Parts extrafields.
 */

/**
 * Implements hook_field_extra_fields().
 * @return array
 */
function commerce_parts_field_extra_fields() {
  return array(
    'commerce_product' => array(
      'part' => array(
        'display' => array(
          'product_image' => array(
            'label' => t('Commerce product: image.'),
            'description' => t('Commerce product image.'),
            'callback' => 'commerce_parts_product_image',
            'weight' => 0,
          ),
          'product_linked_title' => array(
            'label' => t('Commerce product: linked title'),
            'description' => t('Commerce product linked title.'),
            'callback' => 'commerce_parts_product_linked_title',
            'weight' => 0,
          ),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_entity_view().
 * @param $entity
 * @param $entity_type
 * @param $view_mode
 * @param $langcode
 */
function commerce_parts_entity_view($entity, $entity_type, $view_mode, $langcode) {
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);
  $extra_fields = field_extra_fields_get_display($entity_type, $bundle, $view_mode);

  //Image icon for product.
  if (($bundle == 'part') && isset($extra_fields['product_image']) && $extra_fields['product_image']['visible']) {
    $content = commerce_parts_build_extra_fields($entity, 'product_image');
    $entity->content['product_image'] = array(
      '#markup' => $content,
      '#weight' => $extra_fields['product_image']['weight'] + 1000,
    );
  }

  //Product linked title if backreference exists.
  if (($bundle == 'part') && isset($extra_fields['product_linked_title']) && $extra_fields['product_linked_title']['visible']) {
    $content = commerce_parts_build_extra_fields($entity, 'product_linked_title');
    $entity->content['product_linked_title'] = array(
      '#markup' => $content,
      '#weight' => $extra_fields['product_linked_title']['weight'] + 1000,
    );
  }
}

/**
 * Extra field views integration callback.
 * See commerce_parts_field_extra_fields().
 * @param $node
 * @return string
 */
function commerce_parts_product_image($entity) {
  return commerce_parts_build_extra_fields($entity, 'product_image');
}

/**
 * Extra field views integration callback.
 * See commerce_parts_field_extra_fields().
 * @param $node
 * @return string
 */
function commerce_parts_product_linked_title($entity) {
  return commerce_parts_build_extra_fields($entity, 'product_linked_title');
}

/**
 * Calcualte extra fields value.
 * See commerce_catalog_field_extra_fields().
 * @param $node
 * @param string $key
 * @return mixed|string
 */
function commerce_parts_build_extra_fields($entity, $key = 'product_image') {
  $extra_content = '';
  $node = commerce_parts_node_backreference('detail', 'field_part_reference', $entity->product_id);
  switch ($key) {
    case 'product_image':
      if (isset($node->field_image[LANGUAGE_NONE][0]['uri'])) {
        $extra_content = l(
          '<i></i>',
          file_create_url($node->field_image[LANGUAGE_NONE][0]['uri']),
          array(
            'attributes' => array(
              'class' => array('colorbox', 'init-colorbox', 'cboxElement'),
              'title' => $node->field_image['und'][0]['title'],
              'rel' => 'gal',
            ),
            'html' => TRUE
          )
        );
      } else {
        $extra_content = '<i class="empty"></i>';
      }
    break;
    case 'product_linked_title':
      $extra_content = ($node)?l($entity->title, 'node/' . $node->nid):$entity->title;
    break;
  }

  return $extra_content;
}

function commerce_parts_node_backreference($bundle, $field, $product_id) {
  $query = new EntityFieldQuery;

  $query->entityCondition('entity_type', 'node', '=')
    ->propertyCondition('type', $bundle)
    ->fieldCondition($field, 'product_id', $product_id, '=')
    ->range(0, 1);

  if ($result = $query->execute()) {
    $nids = array_keys($result['node']);

    if ($nid = array_shift($nids)) {
      $node = node_load($nid);
      return $node;
    }
  }

  return;
}