<?php
/**
 * @file
 * commerce_parts_importers.feeds_importer_default.inc
 */

/**
 * hook_feeds_importer_default()
 * Define default importer.
 */
function commerce_parts_feeds_importer_default() {
  $export = array();

  $config = array(
    'name' => 'Parts import',
    'description' => 'Импорт запчастей',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsCommerceProductProcessor',
      'config' => array(
        'product_type' => 'part',
        'author' => '1',
        'tax_rate' => '',
        'mappings' => array(
          0 => array(
            'source' => 'sku',
            'target' => 'sku',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'field_sku',
            'target' => 'field_sku',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'distributor',
            'target' => 'field_distributor_reference',
            'term_search' => '1',
            'autocreate' => 0,
          ),
          4 => array(
            'source' => 'price_base',
            'target' => 'commerce_price:amount',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'retail_price',
            'target' => 'field_retail_price:amount',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'trade_price',
            'target' => 'field_trade_price:amount',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'stock',
            'target' => 'field_text_stock',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 1,
        'bundle' => 'part',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );

  if ($distributor_vocabulary = taxonomy_vocabulary_machine_name_load(DISTRIBUTOR_MACHINE_NAME)) {
    $distributors = taxonomy_term_load_multiple(array(), array('vid' => $distributor_vocabulary->vid));

    foreach ($distributors as $distributor) {
      $feeds_importer = new stdClass;
      $feeds_importer->disabled = FALSE;
      $feeds_importer->api_version = 1;
      $feeds_importer->id = 'parts_import_' . $distributor->tid;
      $feeds_importer->config = $config;
      $feeds_importer->config['name'] = t('Parts import #@tid', array('@tid' => $distributor->tid));
      $feeds_importer->config['description'] = t('Parts import. Distributor#@tid (@name)', array('@tid' => $distributor->tid, '@name' => $distributor->name));
      $feeds_importer->config['distributor_id'] = $distributor->tid;

      $export['parts_import_' . $distributor->tid] = $feeds_importer;
    }

    return $export;
  }
}
