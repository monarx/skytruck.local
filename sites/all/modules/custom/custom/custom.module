<?php

/**
 * @file
 * Custom hook, preprocess and theme functions.
 */

module_load_include('inc', 'custom', 'custom.extra_fields');

/**
 * Implements hook_module_implements_alter().
 * Set our form_alter hook last.
 */
function custom_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter') {
    $group = $implementations['custom'];
    unset($implementations['custom']);
    $implementations['custom'] = $group;
  }
}

/**
 * Implements hook_preprocess_html().
 */
function custom_preprocess_html(&$variables) {
  if (variable_get('admin_theme') == $GLOBALS['theme_key']) {
    drupal_add_css(drupal_get_path('module', 'custom') . '/css/admin-inject.css', array('weight' => CSS_THEME));
  }
}

/**
 * Implements hook_feeds_after_save().
 * @param FeedsSource $source
 * @param $entity
 * @param $item
 * @param $entity_id
 */
function custom_feeds_after_save(FeedsSource $source, $entity, $item, $entity_id) {
  if ($source->importer->id == 'users_import') {
    db_update('users')
      ->fields(array('pass' => $item['pass'], 'access' => $item['access'], 'login' => $item['login'], 'init' => $item['init']))
      ->condition('uid', $entity->uid)
      ->execute();
  }
}

/**
 * Implements hook_entity_info_alter().
 * @param $entity_info
 */
function custom_entity_info_alter(&$entity_info) {
  $teaser = array(
    'teaser' => array(
      'label' => t('Teaser'),
      'custom settings' => FALSE,
    )
  );
  $entity_info['taxonomy_term']['view modes'] += $teaser;
  $entity_info['entityform']['view modes'] += $teaser;
}


/**
 * Implements hook_form_FORM_ID_alter().
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function custom_form_alter(&$form, $form_state, $form_id) {
  //Add phone mask.
  if (module_exists('masked_input')) {
    _form_add_mask($form);
  }
}

/**
 * Add mask to form element.
 * @param $element
 */
function form_element_add_mask(&$element, $mask) {
  $process_callbacks = array('masked_input_process_callback', 'ajax_process_form');
  if (isset($element['value']['#process'])) {
    $process_callbacks = array_merge($element['value']['#process'], $process_callbacks);
  }
  $element['value']['#process'] = $process_callbacks;
  $element['value']['#mask'] = $mask;
}


/**
 * Helper function to add mask.
 * @param $element
 * @return array
 */
function _form_add_mask(&$element) {
  $results = array();
  $children = element_children($element);
  foreach($children as $key) {
    $field_info = field_info_field($key);
    if ($field_info['type'] == 'phone' && isset($element[$key][LANGUAGE_NONE][0])) {
      form_element_add_mask($element[$key][LANGUAGE_NONE][0], '+7 (999) 999-9999');
    }
    $child = &$element[$key];
    if (is_array($child)) {
      if (!empty($child['#type']))
        $results[$key] = &$child;
      $results = array_merge($results, _form_add_mask($child));
    }
    unset($child);
  }
  return $results;
}