<?php

/**
 * @file.
 * Implements Node extrafields.
 */

/**
 * Implements hook_field_extra_fields().
 * @return array
 */
function custom_field_extra_fields() {
  return array(
    'node' => array(
      'news' => array(
        'display' => array(
          'prev_next' => array(
            'label' => t('Prev/next'),
            'description' => t('Prev/next links.'),
            'weight' => 0,
          ),
        ),
      ),
    ),
  );
}


/**
 * Implements hook_node_view().
 * @param $node
 * @param $view_mode
 * @param $langcode
 */
function custom_node_view($node, $view_mode, $langcode) {
  $extra_fields = field_extra_fields_get_display('node', $node->type, $view_mode);

  //List sku referenced commerce products.
  if (module_exists('prev_next') && isset($extra_fields['prev_next']) && $extra_fields['prev_next']['visible']) {
    $prev_nid = prev_next_nid($node->nid, 'prev');
    $prev_link = '';
    if ($prev_nid) {
      $node_prev = node_load($prev_nid);
      $prev_link =  l(
        $node_prev->title . '<i class="icon"></i>',
        'node/' . $prev_nid,
        array(
          'html' => true,
          'attributes' => array('class' => 'prev')
        )
      );
    }

    $next_nid = prev_next_nid($node->nid, 'next');
    $next_link = '';
    if ($next_nid) {
      $node_next = node_load($next_nid);
      $next_link = l(
        '<i class="icon"></i>' . $node_next->title,
        'node/' . $next_nid,
        array(
          'html' => true,
          'attributes' => array('class' => 'next')
        )
      );
    }

    $node->content['prev_next'] = array(
      '#markup' => '<div class="news-pager text-center">' .  $next_link . $prev_link . '</div>',
      '#weight' => $extra_fields['prev_next']['weight'] + 1000,
    );
  }
}