(function ($) {
  Drupal.behaviors.ModalClose = {
    attach: function (context, settings) {
      $('#modalBackdrop').click(function() {
        Drupal.CTools.Modal.dismiss();
      });
    }
  }
})(jQuery);